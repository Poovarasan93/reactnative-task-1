import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image, TextInput } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import auth from '@react-native-firebase/auth';

const Sign = ({ navigation }) => {


  const [name, setName] = React.useState('')
  const [mail, setMail] = React.useState('')
  const [pass, setPass] = React.useState('')


  const Validate = () => {
    if (name === "") {
      alert('Please fill the Username')
    }
    else if (mail === "") {
      alert('Please fill the E-Mail Id')
    }
    else if (pass === "") {
      alert('Please fill the Password')
    }
    else {
      auth()
        .createUserWithEmailAndPassword(mail, pass)
        .then(() => {
          console.log('User account created & signed in!');
        })
        .catch(error => {
          if (error.code === 'auth/email-already-in-use') {
            console.log('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            console.log('That email address is invalid!');
          }

          console.error(error);
        });
    }
  }

  return (
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <Text style={styles.icon2} >
          <Icon name='chevron-left' color={'black'} size={30} onPress={() => navigation.navigate('Login')} />
        </Text>
        <View style={styles.flex1}>
          <Text style={styles.text1}>Hi !</Text>
          <Text style={styles.text2}>Create a new account</Text>

          <TextInput
            onChangeText={(text) => setName(text)}
            style={styles.textinput} placeholder="Your Name" />

          <TextInput
            onChangeText={(text) => setMail(text)}
            style={styles.textinput} placeholder="Your E-Mail" />

          <TextInput
            onChangeText={(text) => setPass(text)}
            style={styles.textinput} placeholder="Your Password" secureTextEntry={true} />

          <TouchableOpacity
            onPress={Validate}
            style={styles.button}>

            <Text style={styles.input}>SIGN UP</Text>
          </TouchableOpacity>

          <Text style={styles.text4}>Social Media Login</Text>

          <Text style={styles.icon1} >
            <Icon name='facebook-square' color={'blue'} size={50} />
            <Icon name='github' color={'black'} size={50} />
            <Icon name='twitter' color={'skyblue'} size={50} />
          </Text>
          <Text style={styles.text5}>Don't have any account? <Text style={styles.text6}
            onPress={() => navigation.navigate('Flat')} >Signup</Text></Text>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  flex1: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
    margin: 50,
    marginTop: 10,
    width: 250,
  },
  input: {
    textAlign: "center",
    fontSize: 15,
    color: 'white'
  },
  textinput: {
    alignSelf: 'stretch',
    height: 40,
    marginBottom: 55,
    borderBottomWidth: 2,
    fontSize: 15,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
    borderWidth: 2,
    width: 130,
    alignSelf: "center",
  },
  text1: {
    backgroundColor: 'white',
    fontWeight: 'bold',
    fontSize: 30,
    borderColor: 'blue',
    width: 200,
  },
  text2: {
    backgroundColor: 'white',
    fontWeight: 'bold',
    fontSize: 14,
    borderColor: 'blue',
    width: 200,
    color: 'blue',
    marginBottom: 50,
  },
  text4: {
    backgroundColor: 'white',
    padding: 10,
    borderColor: 'blue',
    width: 200,
    alignSelf: "center",
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 15,
    color: 'midnightblue',
    marginBottom: 20,
    marginTop: 20,
  },
  text5: {
    backgroundColor: 'white',
    padding: 10,
    borderColor: 'blue',
    width: 200,
    alignSelf: "center",
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 11,
    color: 'midnightblue',
  },
  text6: {
    backgroundColor: 'white',
    borderColor: 'blue',
    width: 200,
    alignSelf: "center",
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 13,
    color: 'blue',
    fontWeight: 'bold',
    marginTop: 30,
  },
  icon1: {
    alignSelf: 'center',
    marginBottom: 10,
    letterSpacing: 20,
  },
  icon2: {
    marginTop: 25,
    left: 15,
    marginBottom: 20,
  }


});
export default Sign;