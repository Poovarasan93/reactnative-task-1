import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image} from "react-native";


const Flex = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.flex1}>
        <Image source={require('../assets/login.png')}></Image>
      </View>
      <View style={styles.flex2}>
        <Text style={styles.input1}>Hello !</Text>
        <Text>All elements in the document will inherit this font unless they or one of their parents specifies a new rule.</Text>
        <TouchableOpacity
          style={styles.button}>
          <Text style={styles.input2} onPress={() => navigation.navigate('Login')}>LOGIN</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button1}>
          <Text style={styles.input3} onPress={() => navigation.navigate('Sign')}>SIGN UP</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 2,
    padding: 30,
    backgroundColor: 'white',
  },
  flex1: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  flex2: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 10,
  },
  input1: {
    fontWeight: 'bold',
    textAlign: "center",
    fontSize: 30,
  },
  input2: {
    textAlign: "center",
    fontSize: 15,
    color: 'white'
  },
  input3: {
    textAlign: "center",
    fontSize: 15,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
    borderWidth: 2,
    width: 200,
    alignSelf: "center",
    marginTop: 40,
  },
  button1: {
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: 'blue',
    width: 200,
    alignSelf: "center",
    marginTop: 20,
  }

});
export default Flex;