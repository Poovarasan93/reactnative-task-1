import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, Image, TouchableOpacity, ImageBackground, } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Async = () => {
  const [data, setData] = React.useState('')

  const SetItemData = async () => {
    await AsyncStorage.setItem('storeData', '123456')
  }
  const GetItemData = async () => {
    const dt = await AsyncStorage.getItem('fff')
    console.log(dt)
  }
  return (
    <View style={styles.input}>
      <Text>Async Storage</Text>

      <TouchableOpacity
        style={styles.button1}>
        <Text style={styles.input2} onPress={SetItemData}>one</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button2}>
        <Text style={styles.input2} onPress={GetItemData}>two</Text>
      </TouchableOpacity>

    </View>
  )
}
export default Async;