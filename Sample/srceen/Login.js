import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image, TextInput, Button, Alert } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import auth from '@react-native-firebase/auth';


const Signup = ({ navigation }) => {

  const [mail, setMail] = React.useState('')
  const [pass, setPass] = React.useState('')

  const Validate = () => {

    if (mail === "") {
      alert('Please fill the E-Mail Id')
    }
    else if (pass === "") {
      alert('Please fill the Password')
    }
    else {
      auth()
        .signInWithEmailAndPassword(mail, pass)

        .then(() => {
          console.log('User account created & signed in!');
          navigation.navigate('Loginn');
        })
        .catch(error => {
          if (error.code === 'auth/email-already-in-use') {
            Alert.alert('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            Alert.alert('That email address is invalid!');
          }

          console.error(error);
        });
    }
  }
  return (
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <Text style={styles.icon2} >
          <Icon name='chevron-left' color={'black'} size={30} onPress={() => navigation.navigate('Sign')} />
        </Text>
        <View style={styles.flex1}>
          <Text style={styles.text1}>Welcome !</Text>
          <Text style={styles.text2}>Sign in to continue</Text>


          <TextInput style={styles.textinput} placeholder="Your E-mail"
            onChangeText={(text) => setMail({ text })} />

          <TextInput style={styles.textinput} placeholder="Your Password"
            onChangeText={(text) => setPass({ text })}
            secureTextEntry={true} />

          <TouchableOpacity onPress={Validate}
            style={styles.button}>
            <Text style={styles.input}  >LOGIN</Text>
          </TouchableOpacity>

          <Text style={styles.text3}>Forgot Password ?</Text>
          <Text style={styles.text4}>Social Media Login</Text>
          <Text style={styles.icon} >
            <Icon name='facebook-square' color={'blue'} size={50} />
            <Icon name='github' color={'black'} size={50} />
            <Icon name='twitter' color={'skyblue'} size={50} />
          </Text>
          <Text style={styles.text5}>Don't have any account? <Text style={styles.text6} onPress={() => navigation.navigate('Sign')}>Login</Text></Text>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: 60,
  },
  flex1: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
    padding: 10,
    marginTop: 10,
    width: 250,
  },
  input: {
    textAlign: "center",
    fontSize: 15,
    color: 'white'
  },
  textinput: {
    alignSelf: 'stretch',
    height: 40,
    marginTop: 60,
    borderBottomWidth: 2,
    fontSize: 15,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
    borderWidth: 2,
    marginTop: 55,
    width: 130,
    alignSelf: "center",
  },
  text1: {
    backgroundColor: 'white',
    fontWeight: 'bold',
    fontSize: 30,
    borderColor: 'blue',
    width: 200,
    marginTop: 0,
  },
  text2: {
    backgroundColor: 'white',
    fontWeight: 'bold',
    fontSize: 14,
    borderColor: 'blue',
    width: 200,
    color: 'blue',
  },
  text3: {
    backgroundColor: 'white',
    padding: 10,
    borderColor: 'blue',
    width: 200,
    alignSelf: "center",
    justifyContent: 'center',
    textAlign: 'center',
    marginBottom: 25,
  },
  text4: {
    backgroundColor: 'white',
    padding: 10,
    borderColor: 'blue',
    width: 200,
    alignSelf: "center",
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 15,
    color: 'midnightblue',
    marginBottom: 10,
  },
  text5: {
    backgroundColor: 'white',
    padding: 10,
    borderColor: 'blue',
    width: 200,
    alignSelf: "center",
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 11,
    color: 'midnightblue',
  },
  text6: {
    backgroundColor: 'white',
    borderColor: 'blue',
    width: 200,
    alignSelf: "center",
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 13,
    color: 'blue',
    fontWeight: 'bold',
  },
  icon: {
    alignSelf: 'center',
    marginBottom: 20,
    letterSpacing: 20,
  },
  icon2: {
    marginBottom: 20,
    marginTop: 25,
    left: 15,

  }


});
export default Signup;